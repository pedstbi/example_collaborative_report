## ----helper_function_percent--------------------------------------------------
# percent
#
# function to return a formatted percentage
#
# Usage:
#
#   percent(x, na.rm = FALSE, ...)
#
# Arguments:
#
#       x: binary or logical variable
#   na.rm: logical (default to FALSE), if TRUE ignore NA. see ?mean
#     ...: arguments passed to qwraps2::frmt
#
# Value:
#   a character string
percent <- function(x, na.rm = FALSE, ...) {
  mean(x, na.rm = na.rm) %>%
  magrittr::multiply_by(100) %>%
  qwraps2::frmt(., ...) %>%
  paste0("%")
}

# Example, find the percent of cars wtih a six cyl engine form the mtcars data
mtcars$cyl %>% na.omit %>% table() %>% prop.table
percent(mtcars$cyl == 6, na.rm = TRUE)

## ----helper_function_procedure_summary----------------------------------------
#' @title Procedure Summary
#'
#' @description Find the records for a set of procedure codes in the
#' \code{procedures_raw} or similar \code{data.frame}.
#'
#' @details This function expects that the \code{data.frame} provided contains a
#' column named \code{pid}, a column named \code{proc}, and a column named
#' \code{dos}.
#'
#' Since one of the coluns in the resulting data frame is a list you must have
#' dplyr's namesapce attached in the R session.  If not, the result will not
#' display or will just cause an error.
#'
#' @return A \code{data.frame} with five columns.
#' \describe{
#'   \item{pnewid}{Subject ID from PHIS}
#'   \item{`label`}{The indictor that the code was used.  A useful column when
#'   joining results back into another \code{data.frame}}
#'   \item{`label`_first_dos}{The first day of service}
#'   \item{`label`_total_dos}{The number of unique days the procedure was used}
#'   \item{`label`_doss}{A list of the days of service.}
#' }
#'
#' @param .data the data set
#' @param ... one or more ICD-9 or service codes to search for
#' @param label name of the drug to be used in the returned data frame. This
#' will be the name for the indicator and preface the _first_dos and _total_dos
#'
#'
#' @export
proc_summary <- function(.data, ..., label) {
  codes <- c(...)

  lbl <- deparse(substitute(label))

  dots <- stats::setNames(list( ~ as.integer(any(use)),
                         ~ min(dos),
                         ~ dplyr::n_distinct(dos),
                         ~ I(list(dos))
                         ),
                   c(lbl, paste0(lbl, c("_first_dos",
                                        "_total_dos",
                                        "_doss"))))

  df <- dplyr::mutate_(.data, use = ~ proc %in% codes)
  df <- dplyr::filter_(df, ~ use)
  df <- dplyr::group_by_(df, ~ pid)
  df <- dplyr::summarize_(df, .dots = dots)
  df
} 
