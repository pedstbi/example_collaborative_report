# Example Collaborative Report
This repository is provided as supplementary material for
DeWitt, P. and Bennett T. (2017) "Collaborative Reproducible Reporting: Git
Submodules as a Data Security Solution", a paper presented at HEALTHINF 2017,
the 10th International Conference on Health Informatics.

## Directory Structure

```
  <user path>/example_collaborative_report/
  |-- example_collaborative_data_source/    # git submodule
  |-- products_donotedit/
  |   |-- cache/
  |   |   |-- documentation-analysis/
  |   |   |-- documentation-data-import/
  |   |   `-- manuscript/
  |   |-- documentation-analysis.html
  |   |-- documentation-data-import.html
  |   `-- manuscript.docx
  |-- R-scripts/
  |   |-- analysis.R
  |   |-- data-import.R
  |   |-- helper-functions.R
  |   |-- knit-project1.R
  |   |-- setup.R
  |   `-- table-demographics.R
  |-- yaml-header-files
  |   |-- Arial-12-word-sytle-useme.docx
  |   |-- ncc.csl
  |   `-- reference.bib
  |-- documentation-analysis.Rmd
  |-- documentation-data-import.Rmd
  |-- manuscript.Rmd
  |-- makefile
  `-- README.md
```

The `example_collaborative_data_source` is a submodule containing the data set
and supporting document.  While hosted on bitbucket for this example, the parent
repository could be hosted internally or set up as a bare repo on a network
drive behind your institution's firewall.

The `products_donotedit` directory contains the generated files such as the
manuscript and the documentation of the data import and analysis.  These files
should not be edited by hand, any changes would be over written by the next
build of the project.

`R-scripts` contain all the analysis and data import code.  Most of these files
should be written such that they contain `knitr` chunk identifiers.  This way
one file, one chunk, can be sourced into the manuscript and both documentation
files.  If an edit to the data import is made then the change will appear in all
the needed downstream documents.

`yaml-header-files` are the format templates and bibtex reference files.  These
files are mostly needed for the manuscript but may also be used in other
documentation files.

## Project Root
There are only five files in the root of the project.  All other files are
neatly placed in subdirectories.  The `.Rmd` files are for documentation or the
manuscript.  The make file for building the project, and **this** readme file.


# Building the project:

Two options:

1. Use GNU `make`
2. Source the R script `knit-project1.R` or send to `Rscript` in the
   command line.

## Dependencies

1. [R](www.r-project.org) 
2. See the file `R-scripts/setup.R` for a list of needed packages
3. [pandoc](pandoc.org)