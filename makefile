# Start by determining the Operating system
ifeq ($(shell uname), Linux)
	RHOME    = "/usr/lib/R"
	RSCRIPT := $(RHOME)/bin/Rscript
	R       := $(RHOME)/bin/R
	RM       = /bin/rm -rf
	FixPath  = $1
else
	RHOME    = "C:\Program Files\R"
	RSCRIPT  = $(RHOME)\R-3.3.0\bin\Rscript.exe
	R        = $(RHOME)\R-3.3.0\bin\R.exe 
	RM       = del /Q
	FixPath  = $(subst /,\,$1)
endif

.PHONY: all documentation manuscript clean

all: documentation manuscript

documentation:
	$(RSCRIPT) -e "rmarkdown::render('documentation-data-import.Rmd', output_dir = 'products_donotedit/')"
	$(RSCRIPT) -e "rmarkdown::render('documentation-analysis.Rmd',    output_dir = 'products_donotedit/')"

manuscript:
	$(RSCRIPT) -e "rmarkdown::render('manuscript.Rmd', output_dir = 'products_donotedit/')"

clear_cache:
	/bin/rm -r products_donotedit/cache/*

clean:
